package com.avenuecode.moviescript.services;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.moviescript.domain.Script;
import com.avenuecode.moviescript.exceptions.ScriptAlreadyReceivedException;
import com.avenuecode.moviescript.repositories.ScriptRepository;
import com.avenuecode.moviescript.repositories.SettingRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScriptServiceTest {
	
	@Value("classpath:script-reader-test.txt")
	private Resource script;
	
	@MockBean(name="scriptRepository")
	private ScriptRepository scriptRepository;
	
	@MockBean(name="settingRepository")
	private SettingRepository settingRepository;
	
	@Autowired
	private ScriptService service;
	
	@SuppressWarnings("unchecked")
	@Test
	public void when_script_not_exists_process_successfully() throws ScriptAlreadyReceivedException, IOException {
		service.addScript(IOUtils.toString(script.getInputStream(), "utf-8"));
		
		verify(scriptRepository).save(Mockito.any(Script.class));
		verify(settingRepository).save(Mockito.any(Iterable.class));
	}
	
	@Test(expected = ScriptAlreadyReceivedException.class)
	public void when_script_already_exists_throws_exception() throws ScriptAlreadyReceivedException, IOException {
		when(scriptRepository.exists(Mockito.anyString())).thenReturn(true);
		
		service.addScript(IOUtils.toString(script.getInputStream(), "utf-8"));
		
		fail();
	}

}
