package com.avenuecode.moviescript.controllers;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.exceptions.EntityNotFoundException;
import com.avenuecode.moviescript.repositories.MovieCharacterRepository;
import com.avenuecode.moviescript.services.MovieCharacterService;

@RunWith(SpringRunner.class)
@WebMvcTest(MovieCharacterController.class)
public class MovieCharacterControllerTest {
	
	@MockBean
	private MovieCharacterService service;
	
	@MockBean
	private MovieCharacterRepository characterRepository;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void success_while_finding_all_characters() throws Exception {
		MovieCharacter character = new MovieCharacter(1l, "THREEPIO");
		given(service.findAll()).willReturn(Arrays.asList(character));
		
		mvc.perform(
				get("/characters"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void unexpected_error_while_finding_all_characters() throws Exception {
		given(service.findAll()).willThrow(new RuntimeException());
		
		mvc.perform(
				get("/characters"))
		.andExpect(status().isInternalServerError())
		.andExpect(content().json("{\"message\":\"Unexpected error\"}"));
	}
	
	@Test
	public void success_while_finding_one_character() throws Exception {
		MovieCharacter character = new MovieCharacter(1l, "THREEPIO");
		given(service.findOne(1l)).willReturn(character);
		
		mvc.perform(
				get("/character/1"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void not_found_while_querying_inexistent_character() throws Exception {
		given(service.findOne(1l)).willThrow(new EntityNotFoundException(1l, "character"));
		
		mvc.perform(
				get("/character/1"))
		.andExpect(status().isNotFound())
		.andExpect(content().json("{\"message\":\"Movie character with id 1 not found\"}"));
	}
	
	@Test
	public void unexpected_error_while_finding_one_character() throws Exception {
		given(service.findOne(1l)).willThrow(new RuntimeException());
		
		mvc.perform(
				get("/character/1"))
		.andExpect(status().isInternalServerError())
		.andExpect(content().json("{\"message\":\"Unexpected error\"}"));
	}
	
}
