package com.avenuecode.moviescript.controllers;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.avenuecode.moviescript.domain.Setting;
import com.avenuecode.moviescript.exceptions.EntityNotFoundException;
import com.avenuecode.moviescript.repositories.SettingRepository;
import com.avenuecode.moviescript.services.SettingService;

@RunWith(SpringRunner.class)
@WebMvcTest(SettingController.class)
public class SettingControllerTest {
	
	@MockBean
	private SettingService service;
	
	@MockBean
	private SettingRepository settingRepository;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void success_while_finding_all_settings() throws Exception {
		Setting setting = new Setting(1l, "SPACECRAFT IN SPACE");
		given(service.findAll()).willReturn(Arrays.asList(setting));
		
		mvc.perform(
				get("/settings"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void unexpected_error_while_finding_all_settings() throws Exception {
		given(service.findAll()).willThrow(new RuntimeException());
		
		mvc.perform(
				get("/settings"))
		.andExpect(status().isInternalServerError())
		.andExpect(content().json("{\"message\":\"Unexpected error\"}"));
	}
	
	@Test
	public void success_while_finding_one_setting() throws Exception {
		Setting setting = new Setting(1l, "SPACECRAFT IN SPACE");
		given(service.findOne(1l)).willReturn(setting);
		
		mvc.perform(
				get("/setting/1"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void not_found_while_querying_inexistent_setting() throws Exception {
		given(service.findOne(1l)).willThrow(new EntityNotFoundException(1l, "setting"));
		
		mvc.perform(
				get("/setting/1"))
		.andExpect(status().isNotFound())
		.andExpect(content().json("{\"message\":\"Movie setting with id 1 not found\"}"));
	}
	
	@Test
	public void unexpected_error_while_finding_one_setting() throws Exception {
		given(service.findOne(1l)).willThrow(new RuntimeException());
		
		mvc.perform(
				get("/setting/1"))
		.andExpect(status().isInternalServerError())
		.andExpect(content().json("{\"message\":\"Unexpected error\"}"));
	}
	
}
