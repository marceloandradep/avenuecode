package com.avenuecode.moviescript.controllers;

import static org.mockito.BDDMockito.willThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.avenuecode.moviescript.exceptions.ScriptAlreadyReceivedException;
import com.avenuecode.moviescript.repositories.ScriptRepository;
import com.avenuecode.moviescript.repositories.SettingRepository;
import com.avenuecode.moviescript.services.ScriptService;

@RunWith(SpringRunner.class)
@WebMvcTest(ScriptController.class)
public class ScriptControllerTest {
	
	@MockBean
	private ScriptService service;
	
	@MockBean
	private ScriptRepository scriptRepository;
	
	@MockBean
	private SettingRepository settingRepository;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void success() throws Exception {
		mvc.perform(
				post("/script")
				.contentType(MediaType.TEXT_PLAIN)
				.characterEncoding("utf-8").content("any script"))
		.andExpect(status().isOk())
		.andExpect(content().json("{\"message\":\"Movie script successfully received\"}"));
	}
	
	@Test
	public void when_script_already_received() throws Exception {
		willThrow(ScriptAlreadyReceivedException.class).given(service).addScript("any script");
		
		mvc.perform(
				post("/script")
				.contentType(MediaType.TEXT_PLAIN)
				.characterEncoding("utf-8").content("any script"))
		.andExpect(status().isForbidden())
		.andExpect(content().json("{\"message\":\"Movie script already received\"}"));
	}
	
	@Test
	public void when_an_unexpected_error_occur() throws Exception {
		willThrow(Exception.class).given(service).addScript("any script");
		
		mvc.perform(
				post("/script")
				.contentType(MediaType.TEXT_PLAIN)
				.characterEncoding("utf-8").content("any script"))
		.andExpect(status().isInternalServerError())
		.andExpect(content().json("{\"message\":\"Unexpected error\"}"));
	}

}
