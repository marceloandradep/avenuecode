package com.avenuecode.moviescript.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JsonTest
public class MovieCharacterTest {
	
	@Autowired
    private JacksonTester<MovieCharacter> json;
	
	@Test
	public void when_passing_a_dialogue_string_must_create_a_map_with_number_of_occurences_by_word() {
		MovieCharacter character = new MovieCharacter("Frodo");
		
		character.countDialogue("Hello, world!");
		
		assertThat(character.getWordCount("hello")).isEqualTo(1);
		assertThat(character.getWordCount("world")).isEqualTo(1);
		
		character.countDialogue("Hello, world again!");
		
		assertThat(character.getWordCount("hello")).isEqualTo(2);
		assertThat(character.getWordCount("world")).isEqualTo(2);
		assertThat(character.getWordCount("again")).isEqualTo(1);
	}
	
	@Test
	public void split_dialogue_into_words() {
		String dialogue = "I'm afraid I'm not quite sure, sir. "
				+ "He says \"I found her\", and keeps "
				+ "repeating, \"She's here.\"";
		
		List<String> words = MovieCharacter.splitDialogue(dialogue);
		
		assertThat(words).containsExactly(
				"I'm", "afraid", "I'm", "not", "quite", "sure", "sir", "He",
				"says", "I", "found", "her", "and", "keeps", "repeating", "She's", "here");
	}
	
	@Test
	public void test_serialize() throws IOException {
		MovieCharacter character = new MovieCharacter(1l, "THREEPIO");
		
		character.countDialogue(
				"a a a a a a a a a a " +
				"b b b b b b b b b " +
				"c c c c c c c c " +
				"d d d d d d d " +
				"e e e e e e " +
				"f f f f f " +
				"g g g g " +
				"h h h " +
				"i i " +
				"j k l m");
		
		assertThat(json.write(character)).isEqualToJson("moviecharacter.json");
	}

}
