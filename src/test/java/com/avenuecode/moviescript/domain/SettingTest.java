package com.avenuecode.moviescript.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JsonTest
public class SettingTest {
	
	@Autowired
    private JacksonTester<Setting> json;
	
	@Test
	public void test_serialize() throws IOException {
		Setting setting = new Setting(1l, "REBEL BLOCKADE RUNNER");
		setting.addCharacter(new MovieCharacter(1l, "THREEPIO"));
		
		assertThat(json.write(setting)).isEqualToJson("setting.json");
	}

}
