package com.avenuecode.moviescript.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.domain.Setting;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MovieCharacterRepositoryTest {
	
	@Autowired
	private SettingRepository repo;
	
	@Autowired
	private MovieCharacterRepository characterRepo;
	
	@Test
	public void test() {
		Setting setting = new Setting("SHIRE");
		
		MovieCharacter character = new MovieCharacter("FRODO");
		character.countDialogue("Hello, World!");
		
		setting.addCharacter(character);
		
		setting = repo.save(setting);
		
		character = characterRepo.findOne(1l);
		
		assertThat(character).isNotNull();
		assertThat(character.getName()).isEqualTo("FRODO");
		assertThat(character.getWordCount("hello")).isEqualTo(1);
	}

}
