package com.avenuecode.moviescript.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.domain.Setting;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SettingRepositoryTest {
	
	@Autowired
	private SettingRepository repo;
	
	@Test
	public void querying_existing_setting() {
		Setting setting = new Setting("SHIRE");
		MovieCharacter character = new MovieCharacter("FRODO");
		
		setting.addCharacter(character);
		
		setting = repo.save(setting);
		setting = repo.findOne(setting.getId());
		
		assertThat(setting.getName()).isEqualTo("SHIRE");
		
		character = setting.getCharacter("FRODO");
		
		assertThat(character).isNotNull();
	}
	
	@Test
	public void querying_unexisting_setting() {
		Setting setting = repo.findOne(99l);
		assertThat(setting).isNull();
	}

}
