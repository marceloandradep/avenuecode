package com.avenuecode.moviescript.parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.domain.Setting;

public class ParserTest {
	
	@Test
	public void parsing_test() throws IOException {
		InputStream inputStream = ParserTest.class.getResourceAsStream("/script-reader-test.txt");		
		
		Parser parser = new Parser();
		parser.parse(IOUtils.toString(inputStream, "utf-8"));
		
		Collection<Setting> settings = parser.getSettings();
		
		assertThat(settings.size()).isEqualTo(1);
		
		Setting setting = settings.iterator().next();
		MovieCharacter character = setting.getCharacter("THREEPIO");
		
		assertThat(character).isNotNull();
		assertThat(character.getWordCount("this")).isEqualTo(2);
	}

}
