package com.avenuecode.moviescript.parser;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class ParsingUtilsTest {
	
	private ParsingUtils parsingUtils;
	
	@Before
	public void setup() {
		parsingUtils = new ParsingUtils();
	}
	
	@Test
	public void when_line_starts_with_setting_prefix_line_is_setting() {
		String setting1 = "INT. REBEL BLOCKADE RUNNER";
		String setting2 = "EXT. SPACECRAFT IN SPACE";
		String setting3 = "INT./EXT. LUKE'S SPEEDER - DESERT WASTELAND - TRAVELING - DAY";
		
		assertThat(parsingUtils.isSetting(setting1)).isTrue();
		assertThat(parsingUtils.isSetting(setting2)).isTrue();
		assertThat(parsingUtils.isSetting(setting3)).isTrue();
	}
	
	@Test
	public void when_line_does_not_start_with_setting_prefix_line_is_not_setting() {
		String setting1 = "Random String";
		
		assertThat(parsingUtils.isSetting(setting1)).isFalse();
	}
	
	@Test
	public void when_line_have_single_name_consider_the_name_and_ignore_prefix() {
		String setting = "INT. REBEL BLOCKADE RUNNER";
		
		String name = parsingUtils.getSettingName(setting);
		
		assertThat(name).isEqualTo("REBEL BLOCKADE RUNNER");
	}
	
	@Test
	public void when_line_have_multiple_names_only_consider_the_first_setting_name_and_ignore_prefix() {
		String setting = "INT./EXT. LUKE'S X-WING FIGHTER - COCKPIT - DESERT WASTELAND - TRAVELING - DAY";
		String name = parsingUtils.getSettingName(setting);
		
		assertThat(name).isEqualTo("LUKE'S X-WING FIGHTER");
	}
	
	@Test
	public void when_line_is_idented_by_22_space_chars_line_is_character() {
		String character = "                      LUKE";
		assertThat(parsingUtils.isCharacter(character)).isTrue();
	}
	
	@Test
	public void when_line_is_not_idented_by_22_space_chars_line_is_not_character() {
		String character = "LUKE";
		assertThat(parsingUtils.isCharacter(character)).isFalse();
	}
	
	@Test
	public void when_line_is_idented_by_22_space_chars_ignore_spaces_and_return_character_name() {
		String character = "                      LUKE";
		
		String name = parsingUtils.getCharacterName(character);
		
		assertThat(name).isEqualTo("LUKE");
	}
	
	@Test
	public void when_line_is_idented_by_10_space_chars_line_is_dialogue() {
		String dialogue = "          Red Eleven standing by.";
		assertThat(parsingUtils.isDialogue(dialogue)).isTrue();
	}
	
	@Test
	public void when_line_is_not_idented_by_10_space_chars_line_is_not_dialogue() {
		String dialogue = "               Some random string.";
		assertThat(parsingUtils.isDialogue(dialogue)).isFalse();
	}
	
	@Test
	public void when_line_is_idented_by_10_space_chars_ignore_spaces_and_return_dialogue_line() {
		String dialogue = "          Red Eleven standing by.";
		String diologueLine = parsingUtils.getDialogue(dialogue);
		
		assertThat(diologueLine).isEqualTo("Red Eleven standing by.");
	}

}
