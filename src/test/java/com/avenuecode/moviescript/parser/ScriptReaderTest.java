package com.avenuecode.moviescript.parser;

import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.Mockito;

import com.avenuecode.moviescript.parser.reader.ScriptReader;
import com.avenuecode.moviescript.parser.reader.ScriptReaderObserver;

public class ScriptReaderTest {
	
	@Test
	public void event_dispatching_while_reading_file() throws IOException {
		InputStream inputStream = ParserTest.class.getResourceAsStream("/script-reader-test.txt");
		
		ScriptReaderObserver observer = Mockito.mock(ScriptReaderObserver.class);
		
		ScriptReader reader = new ScriptReader(observer);
		reader.read(IOUtils.toString(inputStream, "utf-8"));
		
		verify(observer).setting("REBEL BLOCKADE RUNNER");
		verify(observer, Mockito.times(3)).character("THREEPIO");
		verify(observer).dialogue("Did you hear that? They've shut down");
		verify(observer).dialogue("the main reactor. We'll be destroyed");
		verify(observer).dialogue("for sure. This is madness!");
		verify(observer).dialogue("We're doomed!");
		verify(observer).dialogue("There'll be no escape for the Princess");
		verify(observer).dialogue("this time.");
	}

}
