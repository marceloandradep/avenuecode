package com.avenuecode.moviescript.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MapKey;

@Entity
public class Setting {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	@ManyToMany(cascade = CascadeType.ALL)
    @MapKey(name="name")
	private Map<String, MovieCharacter> characters = new HashMap<>();
	
	public Setting() {
		this(null, null);
	}
	
	public Setting(String settingName) {
		this(null, settingName);
	}
	
	public Setting(Long id, String settingName) {
		this.id = id;
		this.name = settingName;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public MovieCharacter getCharacter(String characterName) {
		return characters.get(characterName);
	}
	
	public boolean containsCharacter(String characterName) {
		return characters.containsKey(characterName);
	}

	public void addCharacter(MovieCharacter character) {
		characters.put(character.getName(), character);
	}
	
	public Collection<MovieCharacter> getCharacters() {
		return characters.values();
	}

}
