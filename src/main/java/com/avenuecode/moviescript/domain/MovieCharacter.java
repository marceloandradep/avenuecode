package com.avenuecode.moviescript.domain;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;

import com.avenuecode.moviescript.util.WordCountSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class MovieCharacter {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String name;
	
	@ElementCollection
	@MapKeyColumn(name="word")
	@Column(name="count")
	@CollectionTable(name="characterwords")
	private Map<String, Integer> wordCounting = new HashMap<>();
	
	public MovieCharacter() {
		this(null, null);
	}
	
	public MovieCharacter(String characterName) {
		this(null, characterName);
	}
	
	public MovieCharacter(Long id, String characterName) {
		this.id = id;
		this.name = characterName;
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public void countDialogue(String dialogue) {
		splitDialogue(dialogue).stream()
			.map(word -> word.toLowerCase())
			.forEach(word -> wordCounting.merge(word, 1, Integer::sum));
	}
	
	public static List<String> splitDialogue(String dialogue) {
		LinkedList<String> words = new LinkedList<>();
		
		Pattern p = Pattern.compile("\\w+'*\\w*");
		Matcher m = p.matcher(dialogue);
		
		while (m.find()) {
			words.add(m.group());
		}
		
		return words;
	}

	public int getWordCount(String word) {
		Integer count = wordCounting.get(word);
		return count != null ? count : 0;
	}
	
	@JsonSerialize(using = WordCountSerializer.class)
	public Map<String, Integer> getWordCounts() {
		return wordCounting;
	}

}
