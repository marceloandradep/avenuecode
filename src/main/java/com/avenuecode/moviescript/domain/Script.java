package com.avenuecode.moviescript.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Script {
	
	@Id
	private String md5;
	
	public Script() {
		this(null);
	}
	
	public Script(String md5) {
		this.md5 = md5;
	}

	public String getMd5() {
		return md5;
	}

}
