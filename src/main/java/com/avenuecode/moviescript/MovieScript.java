package com.avenuecode.moviescript;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieScript {
	
	public static void main(String[] args) {
		SpringApplication.run(MovieScript.class, args);
	}
	
}
