package com.avenuecode.moviescript.util;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class WordCountSerializer extends JsonSerializer<Map<String, Integer>> {

	@Override
	public void serialize(Map<String, Integer> map, JsonGenerator gen, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		
		gen.writeStartArray();
		
		map.entrySet().stream()
		.sorted((e1, e2) -> Integer.compare(e2.getValue(), e1.getValue()))
		.limit(10)
		.forEach(e -> {
			try {
				gen.writeStartObject();
				gen.writeStringField("word", e.getKey());
				gen.writeNumberField("count", e.getValue());
				gen.writeEndObject();
			} catch (Exception ex) {
				throw new RuntimeException("Error while serializing word count mapping.", ex);
			}
		});
		
		gen.writeEndArray();
	}

}
