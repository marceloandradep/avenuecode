package com.avenuecode.moviescript.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParsingUtils {
	
	private static final Pattern SETTING_REGEX = Pattern.compile("(EXT\\.|INT\\.|INT\\./EXT\\.) (.+?)( - .+|$)");
	private static final Pattern CHARACTER_REGEX = Pattern.compile("\\s{22}(\\w.*)$");
	private static final Pattern DIALOG_REGEX = Pattern.compile("\\s{10}(\\w.*)$");
	
	public boolean isSetting(String line) {
		return line.matches(SETTING_REGEX.pattern());
	}
	
	public boolean isCharacter(String line) {
		return line.matches(CHARACTER_REGEX.pattern());
	}
	
	public boolean isDialogue(String line) {
		return line.matches(DIALOG_REGEX.pattern());
	}
	
	public String getSettingName(String line) {
		Matcher matcher = SETTING_REGEX.matcher(line);
		
		if (matcher.matches()) {
			return matcher.group(2).trim();
		}
		
		throw new IllegalArgumentException("Line does not match the setting pattern.");
	}

	public String getCharacterName(String line) {
		Matcher matcher = CHARACTER_REGEX.matcher(line);
		
		if (matcher.matches()) {
			return matcher.group(1).trim();
		}
		
		throw new IllegalArgumentException("Line does not match the character pattern.");
	}
	
	public String getDialogue(String line) {
		Matcher matcher = DIALOG_REGEX.matcher(line);
		
		if (matcher.matches()) {
			return matcher.group(1).trim();
		}
		
		throw new IllegalArgumentException("Line does not match the dialogue pattern.");
	}

}
