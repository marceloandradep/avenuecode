package com.avenuecode.moviescript.parser;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.domain.Setting;
import com.avenuecode.moviescript.parser.reader.ScriptReader;
import com.avenuecode.moviescript.parser.reader.ScriptReaderObserver;

/**
 * Parsing entry point. Listens for SpringReader events in order
 * to parse the text file.
 * 
 * @author Marcelo
 *
 */
public class Parser implements ScriptReaderObserver {
	
	private Map<String, Setting> settings = new HashMap<>();
	private Map<String, MovieCharacter> characters = new HashMap<>();
	
	/**
	 * Current setting being processed
	 */
	private Setting currentSetting;
	
	/**
	 * Current character being processed
	 */
	private MovieCharacter currentCharacter;
	
	public void parse(String source) {
		ScriptReader reader = new ScriptReader(this);
		reader.read(source);
	}
	
	public Collection<Setting> getSettings() {
		return settings.values();
	}

	@Override
	public void setting(String settingName) {
		Setting setting = settings.get(settingName);
		
		if (setting == null) {
			setting = new Setting(settingName);
			settings.put(settingName, setting);
		}
		
		currentSetting = setting;
	}

	@Override
	public void character(String characterName) {
		MovieCharacter character = characters.get(characterName);
		
		if (character == null) {
			character = new MovieCharacter(characterName);
			characters.put(characterName, character);
		}
		
		if (currentSetting != null && !currentSetting.containsCharacter(characterName)) {
			currentSetting.addCharacter(character);
		}
		
		currentCharacter = character;
	}

	@Override
	public void dialogue(String dialogue) {
		currentCharacter.countDialogue(dialogue);
	}
	
}
