package com.avenuecode.moviescript.parser.reader;

public interface ScriptReaderObserver {
	
	public void setting(String setting);
	public void character(String character);
	public void dialogue(String dialogue);

}
