package com.avenuecode.moviescript.parser.reader;

import java.util.Scanner;

import com.avenuecode.moviescript.parser.ParsingUtils;

/**
 * Reads the text file line by line and emits events
 * according with the type of line:
 * - A setting
 * - A character
 * - Or a dialog 
 * 
 * @author Marcelo
 *
 */
public class ScriptReader {
	
	private ScriptReaderObserver observer;
	private ParsingUtils parsingUtils;
	
	public ScriptReader(ScriptReaderObserver observer) {
		this.observer = observer;
		this.parsingUtils = new ParsingUtils();
	}
	
	public void read(String source) {
		Scanner scanner = new Scanner(source);
		
		while (scanner.hasNext()) {
			String currentLine = scanner.nextLine();
			
			if (parsingUtils.isSetting(currentLine)) {
				observer.setting(parsingUtils.getSettingName(currentLine));
			} else if (parsingUtils.isCharacter(currentLine)) {
				observer.character(parsingUtils.getCharacterName(currentLine));
			} else if (parsingUtils.isDialogue(currentLine)) {
				observer.dialogue(parsingUtils.getDialogue(currentLine));
			}
		}
		
		scanner.close();
	}

}
