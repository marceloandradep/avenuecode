package com.avenuecode.moviescript.repositories;

import org.springframework.data.repository.CrudRepository;

import com.avenuecode.moviescript.domain.Setting;

public interface SettingRepository extends CrudRepository<Setting, Long> {
}
