package com.avenuecode.moviescript.repositories;

import org.springframework.data.repository.CrudRepository;

import com.avenuecode.moviescript.domain.Script;

public interface ScriptRepository extends CrudRepository<Script, String> {
}
