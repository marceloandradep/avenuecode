package com.avenuecode.moviescript.repositories;

import org.springframework.data.repository.CrudRepository;

import com.avenuecode.moviescript.domain.MovieCharacter;

public interface MovieCharacterRepository extends CrudRepository<MovieCharacter, Long> {
}
