package com.avenuecode.moviescript.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.exceptions.EntityNotFoundException;
import com.avenuecode.moviescript.repositories.MovieCharacterRepository;

@Service
public class MovieCharacterService {
	
	@Autowired
	private MovieCharacterRepository characterRepository;
	
	public Iterable<MovieCharacter> findAll() {
		return characterRepository.findAll();
	}
	
	public MovieCharacter findOne(Long id) {
		MovieCharacter character = characterRepository.findOne(id);
		
		if (character != null) {
			return character;
		}
		
		throw new EntityNotFoundException(id, "character");
	}

}
