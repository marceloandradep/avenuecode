package com.avenuecode.moviescript.services;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.logging.Logger;

import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.moviescript.domain.Script;
import com.avenuecode.moviescript.domain.Setting;
import com.avenuecode.moviescript.exceptions.ScriptAlreadyReceivedException;
import com.avenuecode.moviescript.parser.Parser;
import com.avenuecode.moviescript.repositories.ScriptRepository;
import com.avenuecode.moviescript.repositories.SettingRepository;

@Service
public class ScriptService {
	
	private static final Logger log = Logger.getLogger(ScriptService.class.getName());
	
	@Autowired
	private ScriptRepository scriptRepository;
	
	@Autowired
	private SettingRepository settingRepository;
	
	public void addScript(String script) throws ScriptAlreadyReceivedException {
		MessageDigest md5;
		String contentMd5;
		
		try {
			md5 = MessageDigest.getInstance("md5");
			contentMd5 = HexUtils.toHexString(md5.digest(script.getBytes("utf-8")));
			
			log.info("Processing script with content-md5: " + contentMd5);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new RuntimeException("Error while calculating MD5 hash of script.");
		}
		
		if (scriptRepository.exists(contentMd5)) {
			log.warning("Script already exists: content-md5 " + contentMd5);
			throw new ScriptAlreadyReceivedException();
		}
		
		Parser parser = new Parser();
		parser.parse(script);
		
		Collection<Setting> settings = parser.getSettings();
		
		settingRepository.save(settings);
		scriptRepository.save(new Script(contentMd5));
		
		log.info("Script " + contentMd5 + " processed successfully.");
	}
	
}
