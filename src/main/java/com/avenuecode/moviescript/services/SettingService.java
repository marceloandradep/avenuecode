package com.avenuecode.moviescript.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.moviescript.domain.Setting;
import com.avenuecode.moviescript.exceptions.EntityNotFoundException;
import com.avenuecode.moviescript.repositories.SettingRepository;

@Service
public class SettingService {
	
	@Autowired
	private SettingRepository settingRepository;
	
	public Iterable<Setting> findAll() {
		return settingRepository.findAll();
	}
	
	public Setting findOne(Long id) {
		Setting setting = settingRepository.findOne(id);
		
		if (setting != null) {
			return setting;
		}
		
		throw new EntityNotFoundException(id, "setting");
	}

}
