package com.avenuecode.moviescript.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.moviescript.domain.MovieCharacter;
import com.avenuecode.moviescript.services.MovieCharacterService;

@RestController
public class MovieCharacterController {
	
	@Autowired
	private MovieCharacterService service;
	
	@RequestMapping("/characters")
	public Iterable<MovieCharacter> findAll() {
		return service.findAll();
	}
	
	@RequestMapping("/character/{id}")
	public MovieCharacter findOne(@PathVariable Long id) {
		return service.findOne(id);
	}
	
}
