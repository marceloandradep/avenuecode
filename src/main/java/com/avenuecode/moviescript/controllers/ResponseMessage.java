package com.avenuecode.moviescript.controllers;

public class ResponseMessage {
	
	private String message;
	
	public ResponseMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

}
