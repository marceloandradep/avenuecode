package com.avenuecode.moviescript.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.moviescript.exceptions.ScriptAlreadyReceivedException;
import com.avenuecode.moviescript.services.ScriptService;

@RestController
public class ScriptController {
	
	@Autowired
	private ScriptService service;
	
	@RequestMapping(path = "/script", method = RequestMethod.POST)
	public ResponseMessage addScript(@RequestBody String script) throws ScriptAlreadyReceivedException {
		service.addScript(script);
		return new ResponseMessage("Movie script successfully received");
	}

}
