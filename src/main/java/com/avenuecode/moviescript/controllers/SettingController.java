package com.avenuecode.moviescript.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.moviescript.domain.Setting;
import com.avenuecode.moviescript.services.SettingService;

@RestController
public class SettingController {
	
	@Autowired
	private SettingService service;
	
	@RequestMapping("/settings")
	public Iterable<Setting> findAll() {
		return service.findAll();
	}
	
	@RequestMapping("/setting/{id}")
	public Setting findOne(@PathVariable Long id) {
		return service.findOne(id);
	}
	
}
