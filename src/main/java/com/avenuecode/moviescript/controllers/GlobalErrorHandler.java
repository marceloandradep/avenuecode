package com.avenuecode.moviescript.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.avenuecode.moviescript.exceptions.EntityNotFoundException;
import com.avenuecode.moviescript.exceptions.ScriptAlreadyReceivedException;

@ControllerAdvice
public class GlobalErrorHandler extends ResponseEntityExceptionHandler {
	
	private static final Logger log = Logger.getLogger(GlobalErrorHandler.class.getName());
	
	@ExceptionHandler(ScriptAlreadyReceivedException.class)
	public @ResponseBody ResponseEntity<?> handleScriptAlreadyExist(HttpServletRequest request, Throwable ex) {
		return ResponseEntity
				.status(HttpStatus.FORBIDDEN)
				.body(new ResponseMessage("Movie script already received"));
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	public @ResponseBody ResponseEntity<?> handleEntityNotFound(HttpServletRequest request, Throwable ex) {
		EntityNotFoundException notFoundEx = (EntityNotFoundException) ex;
		
		return ResponseEntity
				.status(HttpStatus.NOT_FOUND)
				.body(new ResponseMessage("Movie " + notFoundEx.getLabel() + " with id " + notFoundEx.getId() + " not found"));
	}
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseEntity<?> handleUnknownException(HttpServletRequest request, Throwable ex) {
		log.log(Level.SEVERE, ex.toString(), ex);
		
		return ResponseEntity
				.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new ResponseMessage("Unexpected error"));
	}

}
