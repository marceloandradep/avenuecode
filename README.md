# Movie Script

Given a movie script in the format described below, the application will have to extract the movie settings, the characters and the dialogues, identifying the characters that appeared in each setting, and counting the dialogue words for each character.

## Instalation

Download repository.
```
git clone https://marceloandradep@bitbucket.org/marceloandradep/avenuecode.git
```
Cd into repository folder.
```
cd avenuecode
```
Build application package.
```
mvn package
```
Run application.
```
java -jar target\moviescript-1.0.jar
```
You can also use
```
mvn spring-boot:run
```
to build and run the application with a single command.

## How to upload scripts
You can upload scripts sending a HTTP POST to
```
http://localhost:8080/script
```
and sending the script text as the request body. The application will respond with the JSON
```
{ "message": "Movie script successfully received" }
```
## Querying movie settings
To query all the settings send a HTTP GET to
```
http://localhost:8080/settings
```
or you could query for a specific setting passing its ID as a path parameter
```
http://localhost:8080/setting/id
```
## Querying characters
To query all the characters send a HTTP GET to
```
http://localhost:8080/characters
```
or you could query for a specific character passing its ID as a path parameter
```
http://localhost:8080/character/id
```
## Running tests
To run the tests use
```
mvn test
```